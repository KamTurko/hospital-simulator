#include "ward.h"
#include "employee.h"

class Hospital
{
public:
    vector<Ward> list_of_wards;
	vector<Ward_for_sale> list_of_possible_wards;
	vector<Doctor> list_of_free_doctors;
	vector<Doctor> list_of_busy_doctors;
    vector<QString> list_of_possible_doctors;
	vector<Nurse> list_of_free_nurses;
	vector<Nurse> list_of_busy_nurses;

	int money;
	int recovered;
	int died;

    void print();
	void print_possible_wards();
	void print_possible_doctors();
	void load_possible_wards();

	void move(int patient);
	void remove(int patient);
	void exam(int patient);
	void operate(int patient);

	void dismiss(int ward, int patient);
	void end_opertion(int patient);

	void buy_ward(int ward);
	void upgrade_ward(int ward, int size);

	void employ_doctor(int spec);
	void employ_nurse();
};
