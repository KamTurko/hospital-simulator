#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "simulation.h"
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    void clear_ouptut();
    void output();

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    Hospital_Time hospital_time;
    Simulation simulation;

private slots:
    void on_ExamineButton_clicked();

    void on_MoveButton_clicked();

    void on_HelpButton_clicked();

    void on_OperateButton_clicked();

    void on_RemoveButton_clicked();

    void on_EmployButton_clicked();

    void on_BuyButton_clicked();

    void on_UpgradeButton_clicked();

    void on_NextButton_clicked();

    void on_PatientSelector_valueChanged(int arg1);

    void on_OutputText_textChanged();

    void on_OK_button_clicked();

    void on_EmployButton_2_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
