#include "human.h"
#include "disease.h"

class Patient : public Human
{
public:
    Disease disease;
    bool disease_known;     //0 - hospital crew don't know what illness patient has, 1 - know
    float health;           //From 0 to 1 describes how healthy patient is. Patients with lower health needs more medical aid and have bigger chance to die
    bool alive;
	float remaining_operation_time;
	float remaining_recovery_time;
    void random_new();
    void die();
    void print();
	void operation_step();
	void recovery_step();
};
