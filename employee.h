#include "human.h"

class Employee : public Human
{
public:
	int wage;
};

class Doctor :public Employee
{
public:
    QString specialization;
	void random_new();
    void assign_spec(QString spec);
	void print();
};

class Nurse :public Employee
{

public:
    void random_new();
	void print();
};
