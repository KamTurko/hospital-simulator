#include "hospital.h"
using namespace std;

//print status of hospital: list of patients on wards, list of free or busy doctors or nurses
void Hospital::print()
{
	Human *human;
	for (int i = 0; i < list_of_wards.size(); i++)
	{
        //cout << list_of_wards[i].name << " (" << list_of_wards[i].list_of_patients.size() << "/" << list_of_wards[i].capacity << "):" << endl;
        out.append(list_of_wards[i].name);
        out.append(" (");
        out.append(QString::number(list_of_wards[i].list_of_patients.size()));
        out.append("/");
        out.append(QString::number(list_of_wards[i].capacity));
        out.append("):\n");
		if (list_of_wards[i].list_of_patients.size() > 0)
		{
			for (int j = 0; j < list_of_wards[i].list_of_patients.size(); j++)
			{
                //cout << "  " << j + 1 << ". ";
                out.append("  ");
                out.append(QString::number(j+1));
                out.append(". ");
				human = &list_of_wards[i].list_of_patients[j];
				human->print();
			}
		}
		else
        {
                //cout << "  brak pacjentow" << endl;
            out.append("  brak pacjentów\n");
        }

	}
    //cout << "wolni lekarze:" << endl;
    out.append("wolni lekarze:\n");
	if (list_of_free_doctors.size() > 0)
	{
		for (int j = 0; j < list_of_free_doctors.size(); j++)
		{
            //cout << "  " << j + 1 << ". ";
            out.append("  ");
            out.append(QString::number(j+1));
            out.append(". ");
			human = &list_of_free_doctors[j];
			human->print();
		}
	}
	else
    {
        //cout << "  brak lekarzy (niedobrze)" << endl;
        out.append("  brak lekarzy (niedobrze)\n");
    }
    //cout << "zajeci lekarze:" << endl;
    out.append("zajęci lekarze:\n");
	if (list_of_busy_doctors.size() > 0)
	{
		for (int j = 0; j < list_of_busy_doctors.size(); j++)
		{
            //cout << "  " << j + 1 << ". ";
            out.append("  ");
            out.append(QString::number(j+1));
            out.append(". ");
			human = &list_of_busy_doctors[j];
			human->print();
		}
	}
	else
    {
        //cout << "  brak zajetych lekarzy" << endl;
        out.append("  brak zajetych lekarzy\n");
    }
    //cout << "wolni pielegniarze:" << endl;
    out.append("wolni pielegniarze:\n");
	if (list_of_free_nurses.size() > 0)
	{
		for (int j = 0; j < list_of_free_nurses.size(); j++)
		{
            //cout << "  " << j + 1 << ". ";
            out.append("  ");
            out.append(QString::number(j+1));
            out.append(". ");
			human = &list_of_free_nurses[j];
			human->print();
		}
	}
	else
    {
        //cout << "brak pielegniarzy (niedobrze)" << endl;
        out.append("  brak pielegniarzy (niedobrze)\n");
    }
    //cout << "zajeci pielegniarze:" << endl;
    out.append("zajęci pięlegniarze:\n");
	if (list_of_busy_nurses.size() > 0)
	{
		for (int j = 0; j < list_of_busy_nurses.size(); j++)
		{
            //cout << "  " << j + 1 << ". ";
            out.append("  ");
            out.append(QString::number(j+1));
            out.append(". ");
			human = &list_of_busy_nurses[j];
			human->print();
		}
	}
	else
    {
        //cout << "  brak zajetych pielegniarzy" << endl;
        out.append("  brak zajetych pielegniarzy\n");
    }
};

void Hospital::print_possible_doctors()
{
    for(unsigned long int i = 0; i < list_of_possible_doctors.size(); i++)
    {
        //cout << i + 1 << ". " << list_of_possible_doctors[i] << endl;
        out_doctors.append(QString::number(i+1));
        out_doctors.append(". ");
        out_doctors.append(list_of_possible_doctors[i]);
        out_doctors.append("\n");
    }
}

void Hospital::print_possible_wards()
{
    for(unsigned long int i = 0; i < list_of_possible_wards.size(); i++)
    {
        //cout << i + 1 << ". " << list_of_possible_wards[i].name << "  " << list_of_possible_wards[i].price << endl;
        out_wards.append(QString::number(i+1));
        out_wards.append(". ");
        out_wards.append(list_of_possible_wards[i].name);
        out_wards.append(" ");
        out_wards.append(QString::number(list_of_possible_wards[i].price));
        out_wards.append("\n");
    }
}

//load list of possible wards from file
void Hospital::load_possible_wards()
{
	fstream file;
    file.open("C:/Users/Kamil/Documents/Qt/HS_GUI/Resources/possible_wards.txt", std::ios::in | std::ios::out);
	if (file.good() == true)
	{
		while (!file.eof())
		{
			Ward_for_sale w0;
            string r;
            file >> r;
            w0.name = QString::fromStdString(r);
			file >> w0.price;
			list_of_possible_wards.push_back(w0);
		}
		file.close();
	}
	else
    {
        cout << "BŁĄD WCZYTANIA PLIKU Z ODDZIAŁAMI!" << endl;

    }
};

//buy new ward from list of possible wards
void Hospital::buy_ward(int ward)
{
	if(ward > list_of_possible_wards.size() || ward < 0)
	{
        //cout << "nie mozna kupic takiego oddzialu!" << endl;
        out_error.append("nie mozna kupic takiego oddzialu!\n");
		return;
	}
	Ward_for_sale w0 = list_of_possible_wards[ward - 1];
	Ward w1;
	w1.name = w0.name;
	w1.capacity = 20;
	if(w0.price > money) 
	{
        //cout << "za mało pieniędzy na zakup tego oddziału!" << endl;
        out_error.append("za mało pieniędzy na zakup tego oddziału!\n");
		return;
	}
    list_of_wards.push_back(w1);
	money -= w0.price;
    QString new_spec = w0.name;
	int size = new_spec.size();
    new_spec.resize(size-2);
	list_of_possible_doctors.push_back(new_spec);
};

void Hospital::upgrade_ward(int ward, int size)
{
	if(ward > list_of_wards.size() || ward <= 0)
	{
        //cout << "nie ma takiego oddzialu!" << endl;
        out_error.append("nie ma takiego oddzialu!\n");
		return;
	}
	list_of_wards[ward].capacity += size;
	if (size * 1000 > money)
	{
        //cout << "za mało pieniędzy na powiększenie tego oddziału!" << endl;
        out_error.append("za mało pieniędzy na powiększenie tego oddziału!\n");
		return;
	}
	money -= size * 1000;
};

void Hospital::remove(int patient)
{
	if(patient > list_of_wards[0].list_of_patients.size() || patient <= 0)
	{
        //cout << "nie ma takiego pacjenta!" << endl;
        out_error.append("nie ma takiego pacjenta!\n");
		return;
	}
	list_of_wards[0].list_of_patients.erase(list_of_wards[0].list_of_patients.begin() + patient - 1);
};

//employ a new doctor with selected specialization
void Hospital::employ_doctor(int spec)
{
	if(spec > list_of_possible_doctors.size() || spec <= 0)
	{
        //cout << "nie ma takiej specjalizacji!" << endl;
        out_error.append("nie ma takiej specjalizacji!\n");
		return;
	}
	Doctor d0;
    d0.random_new();
	d0.assign_spec(list_of_possible_doctors[spec - 1]);
    if (2 * d0.wage > money)
    {
        //cout << "za mało pieniędzy na zatrudnienie tego lekarza" << endl;
        out_error.append("za mało pieniędzy na zatrudnienie tego lekarza\n");
        return;
    }
	list_of_free_doctors.push_back(d0);

	money -= 2*	d0.wage;
}

//employ a new nurse
void Hospital::employ_nurse()
{
	Nurse n0;
    n0.random_new();
	list_of_free_nurses.push_back(n0);
	if (2 * n0.wage > money)
	{
        //cout << "za mało pieniędzy na zatrudnienie tego pielęgniarza" << endl;
        out_error.append("za mało pieniędzy na zatrudnienie tego pielęgniarza\n");
		return;
	}
	money -= 2*	n0.wage;
}

//move patient from Waiting Room to corresponding ward
void Hospital::move(int patient) 
{
	if(patient > list_of_wards[0].list_of_patients.size() || patient <= 0)
	{
        //cout << "nie ma takiego pacjenta!" << endl;
        out_error.append("nie ma takiego pacjenta!\n");
		return;
	}

	if (list_of_wards[0].list_of_patients[patient - 1].disease_known == false)
	{
        //cout << "Nie mozna leczyc pacjenta ktoremu nie wiadomo co dolega!" << endl;
        out_error.append("Nie mozna leczyc pacjenta ktoremu nie wiadomo co dolega!\n");
		return;
	}

	if (list_of_wards[0].list_of_patients[patient - 1].remaining_operation_time > 0)
	{
        //cout << "Pacjenta nalezy najpierw operowac!" << endl;
        out_error.append("Pacjenta nalezy najpierw operowac!\n");
		return;
	}

	Patient p0 = list_of_wards[0].list_of_patients[patient - 1];

	for (int i = 0; i < list_of_wards.size(); i++)
	{
		if(p0.disease.category[0] == list_of_wards[i].name[0] && p0.disease.category[1] == list_of_wards[i].name[1] && p0.disease.category[2] == list_of_wards[i].name[2])	//checking if right ward exists by checking first 3 letters
		{
			if (list_of_wards[i].list_of_patients.size() < list_of_wards[i].capacity)	//checking if right ward has enough capacity
			{
				if ((list_of_wards[i].list_of_patients.size() + 1) % 10 == 1)	//checking if adding new patient will require additional doctor
				{
					if (list_of_free_doctors.size() > 0)	//checking if there is enough doctors
					{
						for (int j = 0; j < list_of_free_doctors.size(); j++)	
						{
							if (p0.disease.category == list_of_free_doctors[j].specialization)	//checking if right doctor exists or is free
							{
								Doctor d0 = list_of_free_doctors[j];
								list_of_free_doctors.erase(list_of_free_doctors.begin() + j);
								list_of_busy_doctors.push_back(d0);
								break;
							}
							else
							{
                                //cout << "brak odpowiednich wolnych lekarzy" << endl;
                                out_error.append("Brak odpowiednich wolnych lekarzy\n");
								return;
							}
						}
					}
					else
					{
                        //cout << "brak wolnych lekarzy" << endl;
                        out_error.append("Brak wolnych lekarzy\n");
						return;
					}
				}

				if ((list_of_wards[i].list_of_patients.size() + 1) % 5 == 1)	//checking if adding new patient will require additional nurses
				{

					if (list_of_free_nurses.size() > 0)	//checking if there is enough nurses
					{
						Nurse n0 = list_of_free_nurses[0];
						list_of_free_nurses.erase(list_of_free_nurses.begin());
						list_of_busy_nurses.push_back(n0);
					}
					else
					{
                        //cout << "brak wolnych pielegniarzy" << endl;
                        out_error.append("Brak wolnych pielegniarzy\n");
						return;
					}
				}
				list_of_wards[0].list_of_patients.erase(list_of_wards[0].list_of_patients.begin() + patient - 1);
				list_of_wards[i].list_of_patients.push_back(p0);
				return;
			}
			else
			{
                //cout << "brak miejsca na oddziale" << endl;
                out_error.append("brak miejsca na oddziale\n");
				return;
			}
		}
	}
    //cout << "brak odpowiedniego oddzialu!" << endl;
    out_error.append("brak odpowiedniego oddzialu!\n");
	return;
}

void Hospital::exam(int patient)
{
	if (patient > list_of_wards[0].list_of_patients.size() || patient <= 0)
	{
        //cout << "nie ma takiego pacjenta!" << endl;
        out_error.append("nie ma takiego pacjenta!\n");
		return;
	}
	if (list_of_wards[0].list_of_patients[patient - 1].disease_known == true)
	{
        //cout << "choroba pacjenta jest znana!" << endl;
        out_error.append("choroba pacjenta jest znana!\n");
		return;
	}

	Patient p1 = list_of_wards[0].list_of_patients[patient - 1];
	if (list_of_wards[1].list_of_patients.size() < list_of_wards[1].capacity)
	{
		list_of_wards[0].list_of_patients.erase(list_of_wards[0].list_of_patients.begin() + patient - 1);
		list_of_wards[1].list_of_patients.push_back(p1);
	}
	else
    {
        //cout << "brak miejsca na oddziale" << endl;
        out_error.append("brak miejsca na oddziale\n");
    }
	return;
}

void Hospital::operate(int patient)
{
	if (patient > list_of_wards[0].list_of_patients.size() || patient <= 0)
	{
        //cout << "nie ma takiego pacjenta!" << endl;
        out_error.append("nie ma takiego pacjenta!\n");
		return;
	}

	if (list_of_wards[0].list_of_patients[patient - 1].disease_known == false)
	{
        //cout << "Nie mozna operowac pacjenta ktoremu nie wiadomo co dolega!" << endl;
        out_error.append("Nie mozna operowac pacjenta ktoremu nie wiadomo co dolega!\n");
		return;
	}

    if (list_of_wards[0].list_of_patients[patient - 1].remaining_operation_time <= 0.1)
    {
        //cout << "Pacjenta nie trzeba operować!" << endl;
        out_error.append("Pacjenta nie trzeba operować!\n");
        return;
    }

	Patient p0 = list_of_wards[0].list_of_patients[patient - 1];

	for (int i = 0; i < list_of_wards.size(); i++)
	{
		if (p0.disease.category == list_of_wards[i].name)	//checking if right ward exists
		{
			if (list_of_free_doctors.size() > 2)	//checking if there is enough doctors
			{
				Doctor d0; d0.random_new();
				Doctor d1; d1.random_new();
				Doctor d2; d2.random_new();
				int n[3] = { -1, -1, -1 }; // n[0] - anesthesiologist, n[1] - surgeon, n[2] - specialist  // all three needs to be present during operation
				for (int j = 0; j < list_of_free_doctors.size(); j++)
				{
					if ("anestezjolog" == list_of_free_doctors[j].specialization)	//checking if anesthesiologistr exists or is free
					{
						d0 = list_of_free_doctors[j];
						n[0] = j;
					}
					if ("chirurg" == list_of_free_doctors[j].specialization)	//checking if surgeon exists or is free
					{
						d1 = list_of_free_doctors[j];
						n[1] = j;
					}
					if (p0.disease.category == list_of_free_doctors[j].specialization)	//checking if specialist exists or is free
					{
						d2 = list_of_free_doctors[j];
						n[2] = j;
					}
					if (n[0] >= 0 && n[1] >= 0 && n[2] >= 0)	//checking if all three doctors exist or are free
					{							
						//sorting to erase doctor from list starting from biggest number
						for (int y = 0; y < 3; y++)
						{
							for (int z = y + 1; z < 3; z++)
							{
								if (n[y] > n[z])
								{
									int temp = n[y];
									n[y] = n[z];
									n[z] = temp;
								}
							}
						}
						list_of_free_doctors.erase(list_of_free_doctors.begin() + n[2]);
						list_of_free_doctors.erase(list_of_free_doctors.begin() + n[1]);
						list_of_free_doctors.erase(list_of_free_doctors.begin() + n[0]);
						list_of_busy_doctors.push_back(d0);
						list_of_busy_doctors.push_back(d1);
						list_of_busy_doctors.push_back(d2);
						break;
					}
				}
			}
			else
			{
                //cout << "brak wolnych lekarzy" << endl;
                out_error.append("brak wolnych lekarzy\n");
				return;
			}
		

			if (list_of_free_nurses.size() > 0)	//checking if there is enough nurses
			{
				Nurse n0 = list_of_free_nurses[0];
				list_of_free_nurses.erase(list_of_free_nurses.begin());
				list_of_busy_nurses.push_back(n0);
			}
			else
			{
                //cout << "brak wolnych pielegniarzy" << endl;
                out_error.append("brak wolnych pielegniarzy\n");
				return;
			}

		list_of_wards[0].list_of_patients.erase(list_of_wards[0].list_of_patients.begin() + patient - 1);
		list_of_wards[2].list_of_patients.push_back(p0);
		return;Doctor d0; d0.random_new();
	
		}

	}
    //cout << "brak odpowiedniego oddzialu!" << endl;
    out_error.append("brak odpowiedniego oddzialu!\n");
	return;
}

void Hospital::dismiss(int ward, int patient)
{
	Patient p0 = list_of_wards[ward].list_of_patients[patient];
	if ((list_of_wards[ward].list_of_patients.size() - 1) % 10 == 0)
	{
		for (int j = 0; j < list_of_busy_doctors.size(); j++)
		{
			if (p0.disease.category == list_of_busy_doctors[j].specialization)
			{
				Doctor d0 = list_of_busy_doctors[j];
				list_of_busy_doctors.erase(list_of_busy_doctors.begin() + j);
				list_of_free_doctors.push_back(d0);
				break;
			}
		}
	}

	if ((list_of_wards[ward].list_of_patients.size() - 1) % 5 == 0)
	{
		Nurse n0 = list_of_busy_nurses[0];
		list_of_busy_nurses.erase(list_of_busy_nurses.begin());
		list_of_free_nurses.push_back(n0);
	}
	money = money + list_of_wards[ward].list_of_patients[patient].disease.operation_time * 400;
	money = money + list_of_wards[ward].list_of_patients[patient].disease.recovery_time * 100;
	list_of_wards[ward].list_of_patients.erase(list_of_wards[ward].list_of_patients.begin() + patient);
	recovered++;
}

void Hospital::end_opertion(int patient)
{
	Patient p0 = list_of_wards[2].list_of_patients[patient];

	Doctor d0, d1, d2;
	int n[3] = { -1, -1, -1 }; // n[0] - anesthesiologist, n[1] - surgeon, n[2] - specialist  // all three are present during operation
	for (int j = 0; j < list_of_busy_doctors.size(); j++)
	{
		if ("anestezjolog" == list_of_busy_doctors[j].specialization)	//checking index of busy anesthesiologist
		{
			d0 = list_of_busy_doctors[j];
			n[0] = j;
		}
		if ("chirurg" == list_of_busy_doctors[j].specialization)	//checking index of busy surgeon
		{
			d1 = list_of_busy_doctors[j];
			n[1] = j;
		}
		if (p0.disease.category == list_of_busy_doctors[j].specialization)	//checking index of busy specialist
		{
			d2 = list_of_busy_doctors[j];
			n[2] = j;
		}
		if (n[0] >= 0 && n[1] >= 0 && n[2] >= 0)	//checking if all three doctors are found
		{
			//sorting to erase doctor from list starting from biggest number
			for (int y = 0; y < 3; y++)
			{
				for (int z = y + 1; z < 3; z++)
				{
					if (n[y] > n[z])
					{
						int temp = n[y];
						n[y] = n[z];
						n[z] = temp;
					}
				}
			}
			list_of_busy_doctors.erase(list_of_busy_doctors.begin() + n[2]);
			list_of_busy_doctors.erase(list_of_busy_doctors.begin() + n[1]);
			list_of_busy_doctors.erase(list_of_busy_doctors.begin() + n[0]);
			list_of_free_doctors.push_back(d0);
			list_of_free_doctors.push_back(d1);
			list_of_free_doctors.push_back(d2);
			break;
		}
	}
	list_of_wards[2].list_of_patients.erase(list_of_wards[2].list_of_patients.begin() + patient);
	list_of_wards[0].list_of_patients.push_back(p0);
}

