QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    disease.cpp \
    employee.cpp \
    hospital.cpp \
    hospital_time.cpp \
    lists.cpp \
    main.cpp \
    mainwindow.cpp \
    patient.cpp \
    simulation.cpp
    disease.cpp
    employee.cpp
    hospital.cpp
    hospital_time.cpp
    lists.cpp
    patient.cpp
    simulation.cpp

HEADERS += \
    disease.h \
    employee.h \
    hospital.h \
    hospital_time.h \
    human.h \
    lists.h \
    main.h \
    mainwindow.h \
    patient.h \
    simulation.h \
    ward.h
    mainwindow.h
    disease.h
    employee.h
    hospital.h
    hospital_time.h
    human.h
    lists.h
    patient.h
    simulation.h
    ward.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
