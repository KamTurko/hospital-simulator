#include "patient.h"

using namespace std;

void Patient::random_new()
{
	int n;
	n = rand() % 2;
	if (n == 0)
		sex = male;
	else
		sex = female;

	n = rand() % list_of_names_male.size();
	if (sex == male)
		name = list_of_names_male[n];
	else
		name = list_of_names_female[n];

	n = rand() % list_of_surnames_male.size();
	if (sex == male)
		surname = list_of_surnames_male[n];
	else
		surname = list_of_surnames_female[n];

	age = rand() % 60 + 20;

	Disease d1 = d1.random_disease();
	disease = d1;

	n = rand() % 10;
	if (n > 7)
		disease_known = false;
	else
		disease_known = true;

	remaining_operation_time = disease.operation_time;
	remaining_recovery_time = disease.recovery_time;

	health = 0.8;

	alive = true;
}

void Patient::print()
{
    //cout << name << " " << surname << " ";
    out.append(name);
    out.append(" ");
    out.append(surname);
    out.append(" ");
	if (sex == male)
    {
        //cout << "mezczyzna ";
        out.append("mezczyzna ");
    }
    else
    {
        //cout << "kobieta ";
        out.append("kobieta ");
    }
    //cout << " " << age << " ";
    out.append(QString::number(age));
    out.append(" ");
	if (disease_known == true)
	{
		switch (disease.intensity) 
		{
        case 0: out.append("lekka "); break;
        case 1: out.append("srednia "); break;
        case 2: out.append("ciezka "); break;
		}
        //cout << " " << disease.name << ", ";
        out.append(disease.name);
        out.append(" ");
	}
	else
	{
		for (int i = 0; i < disease.symptoms.size(); i++)
		{
			if (disease.intensity >= disease.symptoms[i].min_level)
			{
				switch (disease.symptoms[i].severity)
				{
                case 0: out.append("lekka "); break;
                case 1: out.append("srednia "); break;
                case 2: out.append("ciezka "); break;
				}
                //cout << " " << disease.symptoms[i].name << ", ";
                out.append(disease.symptoms[i].name);
                out.append(" ");
			}
		}
	}
    //cout << " pozostalo: ";
    out.append(" pozostało:");
	if(remaining_operation_time > 0)
	{
        //cout << remaining_operation_time << "h operacji i: ";
        out.append(QString::number(remaining_operation_time));
        out.append("h operacji i: ");
	}
    //cout << remaining_recovery_time << "h leczenia" << endl;
    out.append(QString::number(remaining_recovery_time));
    out.append("h leczenia\n");
};

void Patient::operation_step()
{
	remaining_operation_time -= 0.25;
};

void Patient::recovery_step()
{
	remaining_recovery_time -= 0.25;
};

