#include "simulation.h"
using namespace std;

Simulation::Simulation()
{
	Hospital_Time t;
	hospital_time = t;
	hospital.recovered = 0;
	hospital.died = 0;
	load_names();
	load_diseases();
	hospital.load_possible_wards();
    hospital.money = 100000;
	Ward w0, w1, w2, w3;
	w0.name = "Poczekalnia";
	w0.capacity = 30;
	hospital.list_of_wards.push_back(w0);
	w1.name = "Gabinet badan";
	w1.capacity = 10;
	hospital.list_of_wards.push_back(w1);
	w2.name = "Sala operacyjna";
	w2.capacity = 2;
	hospital.list_of_wards.push_back(w2);
	w3.name = "ogolny";
	w3.capacity = 30;
	hospital.list_of_wards.push_back(w3);
	Doctor d0; d0.random_new(); d0.assign_spec("ogolny");
	Doctor d1; d1.random_new(); d1.assign_spec("anestezjolog");
	Doctor d2; d2.random_new(); d2.assign_spec("chirurg");
	hospital.list_of_free_doctors.push_back(d0);
	hospital.list_of_free_doctors.push_back(d1);
	hospital.list_of_free_doctors.push_back(d2);
	for (int i = 0; i < 5; i++)
	{
		Nurse n0; n0.random_new();
		hospital.list_of_free_nurses.push_back(n0);
	}
	hospital.list_of_possible_doctors.push_back("ogolny");
	hospital.list_of_possible_doctors.push_back("anestezjolog");
	hospital.list_of_possible_doctors.push_back("chirurg");

}

void Simulation::load_names()
{
	string r;
    QString q;
	fstream file;
    file.open("C:/Users/Kamil/Documents/Qt/HS_GUI/Resources/names_male.txt", std::ios::in | std::ios::out);
	if (file.good() == true)
	{
		while (!file.eof())
		{
			file >> r;
            q = QString::fromStdString(r);
            list_of_names_male.push_back(q);
		}
		file.close();
	}
	else
		cout << "BŁĄD WCZYTANIA PLIKU Z MĘSKIMI IMIENIAMI!" << endl;;
    file.open("C:/Users/Kamil/Documents/Qt/HS_GUI/Resources/names_female.txt", std::ios::in | std::ios::out);
	if (file.good() == true)
	{
		while (!file.eof())
		{
			file >> r;
            q = QString::fromStdString(r);
            list_of_names_female.push_back(q);
		}
		file.close();
	}
	else
		cout << "BŁĄD WCZYTANIA PLIKU Z DAMSKIMI IMIENIAMI!" << endl;;
    file.open("C:/Users/Kamil/Documents/Qt/HS_GUI/Resources/surnames_male.txt", std::ios::in | std::ios::out);
	if (file.good() == true)
	{
		while (!file.eof())
		{
			file >> r;
            q = QString::fromStdString(r);
            list_of_surnames_male.push_back(q);
		}
		file.close();
	}
	else
		cout << "BŁĄD WCZYTANIA PLIKU Z MĘSKIMI NAZWISKAMI!" << endl;;
    file.open("C:/Users/Kamil/Documents/Qt/HS_GUI/Resources/surnames_female.txt", std::ios::in | std::ios::out);
	if (file.good() == true)
	{
		while (!file.eof())
		{
			file >> r;
            q = QString::fromStdString(r);
            list_of_surnames_female.push_back(q);
		}
		file.close();
	}
	else
		cout << "BŁĄD WCZYTANIA PLIKU Z DAMSKIMI NAZWISKAMI!" << endl;;
}


void Simulation::load_diseases()
{
	fstream file;
	int r, nr, nr_of_symptoms;
    file.open("C:/Users/Kamil/Documents/Qt/HS_GUI/Resources/diseases.txt", std::ios::in | std::ios::out);
	if (file.good() == true)
	{
		string d;
        QString q;
		getline(file, d); //avoiding reading first line of diseases file
		while (!file.eof())
		{
			Disease d1;
            file >> nr;
            file >> d;
            d1.name = QString::fromStdString(d);
            file >> d;
            d1.category = QString::fromStdString(d);
            file >> d1.operation_time;
            file >> d1.recovery_time;
            file >> nr_of_symptoms;
			for (int i = 0; i < nr_of_symptoms; i++)
			{
				Symptom s1;
				file >> r;
				switch (r) {
				case 0: s1.min_level = mild; break;
				case 1: s1.min_level = avarage; break;
				case 2: s1.min_level = severe; break;
				}
                file >> d;
                s1.name = QString::fromStdString(d);
				file >> r;
				switch (r) {
				case 0: s1.severity = mild; break;
				case 1: s1.severity = avarage; break;
				case 2: s1.severity = severe; break;
				}
				d1.symptoms.push_back(s1);
			}
			d1.intensity = mild;
			list_of_diseases.push_back(d1);
		}
		file.close();
	}
	else
		cout << "BLAD WCZYTANIA PLIKU Z CHOROBAMI!" << endl;
}


void Simulation::new_patients()
{
	int n;
	if (hospital_time.get_hour() > 6 && hospital_time.get_hour() < 18)
		n = rand() % 3;
	else
		n = rand() % 2;
	if (n + hospital.list_of_wards[0].list_of_patients.size() > hospital.list_of_wards[0].capacity)
		n = hospital.list_of_wards[0].capacity - hospital.list_of_wards[0].list_of_patients.size();
	for (int i = 0; i < n; i++)
	{
		Patient p;
		p.random_new();
		hospital.list_of_wards[0].list_of_patients.push_back(p);
	}
}



void Simulation::input_loop()
{
	bool next_step = false;
	string command;
	do
	{
		print_status();
		cout << "Co robic? (wpisz \"h\" lub \"help\" by wypisac dostepne komendy)" << endl;
		cin >> command;
		if (command == "help" || command == "h")
		{
			cout << "(h)elp - wypisanie pomocy" << endl;
			cout << "(n)ext - następny krok symulacji" << endl;
			cout << "(m)ove - przeniesienie wybranego pacjenta na odpowiedni oddział" << endl;
			cout << "(ex)amination - zbadanie pacjenta" << endl;
			cout << "(em)ploy - zatrudnienie nowego pracownika" << endl;
			cout << "(o)perate - operacja wybranego pacjenta" << endl;
			cout << "(r)emove - odesłanie wybranego pacjenta do domu lub innego szpitala" << endl;
			cout << "(b)uy - zakup nowego oddziału" << endl;
			cout <<	"(u)pgrade - zwiększenie pojemności wybranego oddziału" << endl;
		}
		else if (command == "move" || command == "m")
		{
			cout << "ktorego pacjenta przeniesc?: " << endl;
			int r;
			cin >> r;
			hospital.move(r);
		}
		else if (command == "examination" || command == "ex")
		{
			cout << "ktorego pacjenta zbadac?: " << endl;
			int r;
			cin >> r;
			hospital.exam(r);
		}		
		else if (command == "operate" || command == "o")
		{
			cout << "ktorego pacjenta operowac?: " << endl;
			int r;
			cin >> r;
			hospital.operate(r);
		}
		else if (command == "buy" || command == "b")
		{
			cout << "ktoru oddzial kupic?: " << endl;
			hospital.print_possible_wards();
			int r;
			cin >> r;
			hospital.buy_ward(r);
		}
		else if (command == "employ" || command == "em")
		{
			cout << "zatrudnić lekarza ((d)octor) czy pielegniarza ((n)urse)?" << endl;
			string r;
			cin >> r;
			if (r == "doctor" || r == "d")
			{
				cout << "jakiej specjalizacji?" << endl;
				hospital.print_possible_doctors();
				int s;
				cin >> s;
				hospital.employ_doctor(s);
			}
			if (r == "nurse" || r == "n")
				hospital.employ_nurse();
		}	
		else if (command == "remove" || command == "r")
		{
			cout << "ktorego pacjenta odeslac do domu lub innego szpitala?: " << endl;
			int r;
			cin >> r;
			hospital.remove(r);
		}
		else if (command == "upgrade" || command == "u")
		{
			cout << "ktory oddzial ulepszyc?: " << endl;
			int r, t;
			cin >> r;
			cout << "o ile miejsc go ulepszyc (koszt 1000 za miejsce)?: ";
			cin >> t;
			hospital.upgrade_ward(r,t);
		}
		else if (command == "next" || command == "n")
		{
			next_step = true;
		}
		else
		{
			cout << "Niepoprawna komenda!" << endl;
		}

	} while (!next_step);
}

void Simulation::step()
{    
	//checking if it's time to pay salary to employees
	if (hospital_time.get_hour() == 0 && hospital_time.get_minute() == 0 && hospital_time.get_day() == 10)
	{
		for (int i = 0; i < hospital.list_of_free_doctors.size(); i++)
			hospital.money -= hospital.list_of_free_doctors[i].wage;
		for (int i = 0; i < hospital.list_of_busy_doctors.size(); i++)
			hospital.money -= hospital.list_of_busy_doctors[i].wage;
		for (int i = 0; i < hospital.list_of_free_nurses.size(); i++)
			hospital.money -= hospital.list_of_free_nurses[i].wage;
		for (int i = 0; i < hospital.list_of_busy_nurses.size(); i++)
			hospital.money -= hospital.list_of_busy_nurses[i].wage;
	}

	//examinating patients
	for (int i = 0; i < hospital.list_of_wards[1].list_of_patients.size(); i++)
	{
		int n = rand() % 2;
		if (n == 0)
		{
			hospital.list_of_wards[1].list_of_patients[i].disease_known = true;
			Patient p1 = hospital.list_of_wards[1].list_of_patients[i];
			hospital.list_of_wards[1].list_of_patients.erase(hospital.list_of_wards[1].list_of_patients.begin() + i);
			hospital.list_of_wards[0].list_of_patients.push_back(p1);
		}
	}

	//operating patients
	for (int i = 0; i < hospital.list_of_wards[2].list_of_patients.size(); i++)
	{
		hospital.list_of_wards[2].list_of_patients[i].operation_step();
		if (hospital.list_of_wards[2].list_of_patients[i].remaining_operation_time < 0.1)
			hospital.end_opertion(i);
	}

	//recovering patients
	for (int i = 3; i < hospital.list_of_wards.size(); i++) //int i = 3, because patients in Waiting Room, Examination Room, and Surgery do not recover
	{
		for (int j = 0; j < hospital.list_of_wards[i].list_of_patients.size(); j++)
		{
			hospital.list_of_wards[i].list_of_patients[j].recovery_step();
			if (hospital.list_of_wards[i].list_of_patients[j].remaining_recovery_time < 0.1)
				hospital.dismiss(i, j);
		}
	}

	//move forward in time
	hospital_time.next();
}

void Simulation::print_status()
{
	cout << "##########################" << endl;
	//system("cls");	//clearing the console the worst way
	hospital_time.print();
	cout << "fundusze: " << hospital.money << " monet" << endl;
	hospital.print();
}
