#include "hospital_time.h"
using namespace std;



Hospital_Time::Hospital_Time(int m, int h, int d, int mn, int y)
{
	min = m;
	hour = h;
	day = d;
	month = mn;
	year = y;
};

void Hospital_Time::next()
{
	min += 15;
	if (min == 60)
	{
		min = 0;
		hour++;
	}
	if (hour == 24)
	{
		hour = 0;
		day++;
	}
	if (day == 31)
	{
		day = 1;
		month++;
	}
	if (month == 13)
	{
		month = 1;
		year++;
	}
};

int Hospital_Time::get_minute()
{
	int new_minute = min;
	return new_minute;
};

int Hospital_Time::get_hour()
{
	int new_hour = hour;
	return new_hour;
};

int Hospital_Time::get_day()
{
	int new_day = day;
	return new_day;
};

void Hospital_Time::print()
{
	if (hour < 10)
    {
		cout << 0;
        out_time.append("0");
    }
	cout << hour << ":";
    out_time.append(QString::number(hour));
    out_time.append(":");
	if (min < 10)
    {
		cout << 0;
        out_time.append("0");
    }
    cout << min << " " << day << "." << month << "." << year << endl;
    out_time.append(QString::number(min));
    out_time.append(" ");
    out_time.append(QString::number(day));
    out_time.append(".");
    out_time.append(QString::number(month));
    out_time.append(".");
    out_time.append(QString::number(year));
    out_time.append("\n");
};
