#ifndef HUMAN
#define HUMAN	

#include "main.h"
#include "lists.h"

enum e_Sex { male, female };

class Human
{
public:
    QString name, surname;
	e_Sex sex;
	int age;
	virtual void random_new() = 0;
	virtual void print() = 0;
};
#endif
