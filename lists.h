#include "main.h"

#ifndef LISTS
#define LISTS	

extern vector<QString> list_of_names_male;
extern vector<QString> list_of_names_female;
extern vector<QString> list_of_surnames_male;
extern vector<QString> list_of_surnames_female;

extern QString out;
extern QString out_time;
extern QString out_doctors;
extern QString out_wards;
extern QString out_error;

#endif
