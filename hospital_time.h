#include "main.h"
#include "lists.h"

class Hospital_Time
{
private:
    int min;
    int hour;
    int day;
    int month;
    int year;
public:
    Hospital_Time(int min=0, int hour=12, int day=1, int month=3, int year=2020);
    void next();
	void print();
	int get_minute();
	int get_hour();
	int get_day();
};
