#include "main.h"
#include "lists.h"

enum e_Intensity { mild = 0, avarage = 1, severe = 2 };

class Symptom
{
public:
	e_Intensity min_level;
    QString name;
	e_Intensity severity;
};

class Disease
{
public:
    QString name;
    QString category;
	float operation_time;
	float recovery_time;
	e_Intensity intensity;
	vector<Symptom> symptoms;
	Disease random_disease();
	void print();
};

extern vector<Disease> list_of_diseases;
