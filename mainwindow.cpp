#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    output();
}

void MainWindow::clear_ouptut()
{
    out.clear();
    ui->label_2->setText(out);
    out_time.clear();
    ui->time_label->setText(out_time);
    out_wards.clear();
    ui->av_ward->setText(out_wards);
    out_doctors.clear();
    ui->av_doctors->setText(out_doctors);
    out_error.clear();
    ui->error_label->setText(out_error);
}
void MainWindow::output()
{
    simulation.hospital_time.print();
    simulation.hospital.print();
    simulation.hospital.print_possible_doctors();
    simulation.hospital.print_possible_wards();

    ui->label_2->setText(out);
    ui->time_label->setText(out_time);
    ui->av_ward->setText(out_wards);
    ui->av_doctors->setText(out_doctors);
    ui->error_label->setText(out_error);

    QString wealth = "pieniądze: ";
    wealth.append(QString::number(simulation.hospital.money));
    ui->money_label->setText(wealth);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::on_MoveButton_clicked()
{
    clear_ouptut();
    simulation.hospital.move(ui->PatientSelector->value());
    output();
}

void MainWindow::on_ExamineButton_clicked()
{
    clear_ouptut();
    simulation.hospital.exam(ui->PatientSelector->value());
    output();
}

void MainWindow::on_HelpButton_clicked()
{
    clear_ouptut();

    output();
}

void MainWindow::on_OperateButton_clicked()
{
    clear_ouptut();
    simulation.hospital.operate(ui->PatientSelector->value());
    output();
}

void MainWindow::on_RemoveButton_clicked()
{
    clear_ouptut();
    simulation.hospital.remove(ui->PatientSelector->value());
    output();
}

void MainWindow::on_EmployButton_clicked()
{
    clear_ouptut();
    simulation.hospital.employ_doctor(ui->PatientSelector->value());
    output();
}

void MainWindow::on_BuyButton_clicked()
{
    clear_ouptut();
    simulation.hospital.buy_ward(ui->PatientSelector->value());
    output();
}

void MainWindow::on_UpgradeButton_clicked()
{
    clear_ouptut();
    //simulation.hospital.upgrade_ward(ui->PatientSelector->value());
    output();
}

void MainWindow::on_NextButton_clicked()
{
    clear_ouptut();

    simulation.step();
    simulation.new_patients();

    output();
}

void MainWindow::on_PatientSelector_valueChanged(int arg1)
{

}

void MainWindow::on_OutputText_textChanged()
{
    clear_ouptut();

    output();
}

void MainWindow::on_OK_button_clicked()
{

}

void MainWindow::on_EmployButton_2_clicked()
{
    clear_ouptut();
    simulation.hospital.employ_nurse();
    output();
}
