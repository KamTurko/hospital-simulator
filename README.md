# Hospital simulator

[PL]
Program napisany w celu zaliczenia przedmiotu Programowanie w języku C++

1.Symulacja wykonywana jest z krokiem 15 minut

2.Co kwadrans dostajemy listę nowych pacjentów którzy:
	-mogą przyjść sami i wykazują jakieś objawy, należy wtedy ich zbadać
	-przyjechali karetką i już wiadomo co im dolega, lub i tak trzeba ich dalej zbadać
(liczba pacjentów którzy się zgłaszają jest różna w czasie, wiadomo że za dnia zgłasza się więcej ludzi niż w nocy)

3.W związku z powyższym pacjenci to "obiekty". Mają swoje dane, wiek, płeć, "zdrowotność" (czyli jak dobrze znoszą choroby) i oczywiście chorobę która ma różny poziom intensywności
	3a.Choroba również jest "obiektem". Ma swoją nazwę, kategorię i przede wszystkim objawy (o różnych poziomach intensywności, przykładowo wiele chorób powoduje gorączkę, lecz jedne mniejszą inne większą) oraz leczenie

4.Pacjenci zdiagnozowani (tzn. których chorobę już znamy) mogą (a nawet powinni) zostać wysłani na leczenie

5.Leczenie przebiega w następujący sposób:
	-pacjent trafia na odpowiedni oddział (np. z rakiem trafia na onkologię)
	-leczenie możemy podzielić na "pośrednie" (pacjent sobie leży i się kuruje) i "bezpośrednie" (operacja)
	-w leczeniu "pośrednim" 1 lekarz może zajmować się 10 pacjentami, zaś 1 pielęgniarka 5
	-w operacjach zawsze potrzebny jest lekarz (w tym zawsze 1 lekarz specjalizacji "anestezjolog", 1 "chirurg" i 1 pielęgniarka)

6.Oba rodzaje leczeń zużywają zasoby (czyli głównie lekarstwa)
	6a.Czas leczenia, zużycie zasobów i szansa przeżycia opiera się o matematyczną formułę której wynik zależy od choroby, jej intensywności, zdrowotnosci pacjenta jak też jego wieku 

8.Wszystkie procesy leczenia trwają trochę czasu, lecz zawsze jest to wielokrotność 15 minut (więc nawet 5 minutowy zabieg trwa kwadrans)

9.Wyleczony pacjent idzie sobie do domu, a na nasze konto bankowe wpływają pieniądze

10.Za pieniądze można kupować więcej lekarstw, nowe oddziały (lub rozbudowywać obecne) oraz personel