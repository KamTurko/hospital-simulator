#include "hospital.h"
#include "hospital_time.h"

class Simulation
{
public:
	Hospital_Time hospital_time;
	Hospital hospital;

	void load_names();
	void load_diseases();
	void new_patients();
	void input_loop();
	void step();
	void print_status();
	Simulation();
};