#include "employee.h"

using namespace std;


void Doctor::random_new()
{
	int n;
	n = rand() % 2;
	if (n == 0)
		sex = male;
	else
		sex = female;

	n = rand() % list_of_names_male.size();
	if (sex == male)
		name = list_of_names_male[n];
	else
		name = list_of_names_female[n];

	n = rand() % list_of_surnames_male.size();
	if (sex == male)
		surname = list_of_surnames_male[n];
	else
		surname = list_of_surnames_female[n];

	age = rand() % 40 + 25;

	wage = 6000;

	specialization = "ogolny";
}

void Doctor::assign_spec(QString spec)
{
	specialization = spec;
}


void Doctor::print()
{
    //cout << name << " " << surname << " ";
    out.append(name);
    out.append(" ");
    out.append(surname);
    out.append(" ");
    if (sex == male)
    {
        //cout << "mezczyzna ";
        out.append("mezczyzna ");
    }
    else
    {
        //cout << "kobieta ";
        out.append("kobieta ");
    }
    //cout << " " << age << " " << specialization << " pensja:" << wage << endl;
    out.append(QString::number(age));
    out.append(" ");
    out.append(specialization);
    out.append(" pensja:");
    out.append(QString::number(wage));
    out.append("\n");
}
;


void Nurse::random_new()
{
	int n;
	n = rand() % 10;
	if (n > 7)
		sex = male;
	else
		sex = female;

	n = rand() % list_of_names_male.size();
	if (sex == male)
		name = list_of_names_male[n];
	else
		name = list_of_names_female[n];

	n = rand() % list_of_surnames_male.size();
	if (sex == male)
		surname = list_of_surnames_male[n];
	else
		surname = list_of_surnames_female[n];

	age = rand() % 40 + 25;

	wage = 4000;
};

void Nurse::print()
{
    //cout << name << " " << surname << " ";
    out.append(name);
    out.append(" ");
    out.append(surname);
    out.append(" ");
    if (sex == male)
    {
        //cout << "mezczyzna ";
        out.append("mezczyzna ");
    }
    else
    {
        //cout << "kobieta ";
        out.append("kobieta ");
    }
    //cout << " " << age << " " << specialization << " pensja:" << wage << endl;
    out.append(QString::number(age));
    out.append(" pensja:");
    out.append(QString::number(wage));
    out.append("\n");
}
;
