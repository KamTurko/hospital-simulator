#include "disease.h"
using namespace std;

Disease Disease::random_disease()
{
	Disease d1;
	int n;
	if (list_of_diseases.size() > 0)
		n = rand() % list_of_diseases.size();
	else
		cout << "ERROR! Nie wczytano pliku z chorobami \n";
	d1 = list_of_diseases[n];
	n = rand() % 3;
	switch (n) {
		case 0: d1.intensity = mild; break;
		case 1: d1.intensity = avarage; break;
		case 2: d1.intensity = severe; break;
	}
	return d1;
};

void Disease::print()
{
    //cout << name << " " << category << " " << operation_time << " " << recovery_time << " " << intensity;
    out.append(name);
    out.append(" ");
    out.append(category);
    out.append(" ");
    out.append(QString::number(operation_time));
    out.append(" ");
    out.append(QString::number(recovery_time));
    out.append(" ");
    out.append(intensity);
    out.append(" ");
	for (int i = 0; i < symptoms.size(); i++)
    {
        //cout << symptoms[i].min_level << " " << symptoms[i].name << " " << symptoms[i].severity << " ";
        out.append(symptoms[i].min_level);
        out.append(" ");
        out.append(symptoms[i].name);
        out.append(" ");
        out.append(symptoms[i].severity);
        out.append(" ");
    }
    out.append("\n");
}

vector<Disease> list_of_diseases;
